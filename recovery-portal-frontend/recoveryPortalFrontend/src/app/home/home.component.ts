import { Component } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  amount: number = 0;
  constructor(
    private toastr: ToastrService,
    public translate: TranslateService
  ) {
    translate.addLangs(['English', 'Tamil']);
    translate.setDefaultLang('English');
  }
  onSubmit() {
    this.toastr.success('success', 'we won');
  }

  //Language Switcher
  swithLang(lang: string) {
    this.translate.use(lang);
  }
}
