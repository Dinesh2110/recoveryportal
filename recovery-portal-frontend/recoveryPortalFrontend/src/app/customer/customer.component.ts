import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
})
export class CustomerComponent implements OnInit {
  constructor(public translate: TranslateService) {
    translate.addLangs(['English', 'Tamil']);
    translate.setDefaultLang('English');
  }
  //Language Switcher
  switchLang(lang: string) {
    this.translate.use(lang);
  }

  ngOnInit(): void {}
  cheak() {}
}
