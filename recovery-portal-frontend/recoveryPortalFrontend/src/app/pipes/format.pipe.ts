import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'format',
})
export class FormatPipe implements PipeTransform {
  transform(value: number) {
    return String(value)
      .replace(/\D/g, '')
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  // transform(value: any, ...args: any[]) {
  //   var num = numberWithCommas(value);
  //   console.log(num);
  //   return num;
  // }
  //   transform(value: number, locale?: string): string {
  //     return new Intl.NumberFormat(locale, {
  //       minimumFractionDigits: 0,
  //     }).format(Number(value));
  //   }
}
// function numberWithCommas(x) {
//   var parts = x.toString().split('.');
//   parts[0] = parts[0].replace(/(?!^)(?=(?:\d{3})+$)/g, ',');
//   //   String(x).replace(
//   //     /(?!^)(?=(?:\d{3})+$)/g,
//   //     ' '
//   // );
//   return parts.join('.');
// }
