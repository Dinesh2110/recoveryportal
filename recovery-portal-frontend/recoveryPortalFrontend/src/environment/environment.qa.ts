export const environment = {
  production: false,
  hostUrl: 'http://qa.site',
  userName: 'userqa',

  port: 8080,
  envName: 'qa',
};
