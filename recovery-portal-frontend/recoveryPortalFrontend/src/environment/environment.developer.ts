export const environment = {
  production: false,
  hostUrl: 'http://dev.site',
  userName: 'userDev',

  port: 8080,
  envName: 'dev',
};
